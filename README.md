### ABOUT: ###

This repository holds the source code for the application "intersections", which implements the code to find the intersections between rectangles defined in a JSON file.

For the JSON parsing I used the library "simplejson" located in https://github.com/MJPA/SimpleJSON.

### BUILDING: ###

Currently, the project only give support for building in Windows platforms through Visual Studio. For this matter, the repository  includes a folder "vs2015" with the solution and project files for the building using VisualStudio 2015 or newer.

### RUNNING: ###

This application is console based, so basically all you need to do is go to the folder where de executable was built and execute it from the console.

The only argument needed by the program is the absolute or relative path to the JSON file wanted to be loaded.


```
#!sh

intersections path_to_json
```
	
For example, from the default output folder for Visual Studio, the command to load the example JSON would be:


```
#!sh

intersections ..\..\..\..\intersections\examples\example.json
```
	
The program would return error if it fails during the parsing of the JSON file or the intersections found during the calculation, or the message "No intersections found" if it isn't any.

### TESTING: ###

This repository also has a folder "examples" with some json test cases for the application.