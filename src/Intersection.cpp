#include "Intersection.h"

Intersection::Intersection()
{
}

Intersection::Intersection(int x, int y, int w, int h) : 
	Rectangle(x, y, w, h)
{
}

Intersection::Intersection(const Rectangle& rect) :
	Rectangle(rect.getX(), rect.getY(), rect.getW(), rect.getH())
{
	_id += rect.getId();
}

Intersection::Intersection(const Rectangle& rectA, const Rectangle& rectB)
{
	Intersection rect = rectA.intersects(rectB);
	if (rect.isValid()) {
		_x = rect.getX();
		_y = rect.getY();
		_w = rect.getW();
		_h = rect.getH();

		_id += rectA.getId();
		_id += rectB.getId();
	}
}

Intersection Intersection::intersects(const Rectangle& rect) const
{
	Intersection intersection = Rectangle::intersects(rect);

	intersection.addId(_id);

	return intersection;
}

void Intersection::addId(IDComponent id)
{
	_id += id;
}

IDComponent Intersection::getId()
{
	return _id;
}
