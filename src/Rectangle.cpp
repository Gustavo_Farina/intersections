#include "Rectangle.h"

#include "Intersection.h"

#include <assert.h>
#include <algorithm>
#include <iostream>

Rectangle::Rectangle()
{
}

Rectangle::Rectangle(IDComponent id)
{
	_id = id;
}

Rectangle::Rectangle(const Rectangle & rect) :
	RectangleBase(rect.getX(), rect.getY(), rect.getW(), rect.getH())
{
	_id = rect.getId();
}

Rectangle::Rectangle(int x, int y, int w, int h) :
	RectangleBase(x, y, w, h)
{
}

Rectangle::Rectangle(IDComponent id, int x, int y, int w, int h) :
	RectangleBase(x, y, w, h)
{
	_id = id;
}

Intersection Rectangle::intersects(const Rectangle& rect) const
{
	if ((_x > rect.getX2() || getX2() < rect.getX()) ||
		(_y > rect.getY2() || getY2() < rect.getY())) // There is not intersection
		return Intersection();

	int intersection_x, intersection_y, intersection_w, intersection_h;

	intersection_x = std::max(_x, rect.getX());
	intersection_y = std::max(_y, rect.getY());
	intersection_w = std::abs(std::max(_x, rect.getX()) - std::min(getX2(), rect.getX2()));
	intersection_h = std::abs(std::max(_y, rect.getY()) - std::min(getY2(), rect.getY2()));

	Intersection intersection(Rectangle(_id, intersection_x, intersection_y, intersection_w, intersection_h));
	intersection.addId(rect.getId());
	return intersection;
}