#pragma once

class RectangleBase
{
public:
	RectangleBase();
	
	RectangleBase(const RectangleBase& rect);

	RectangleBase(int x, int y, int w, int h);

	bool isValid();

	int getX() const { return _x; }
	int getY() const { return _y; }
	int getW() const { return _w; }
	int getH() const { return _h; }

	int getX2() const { return _x + _w; }
	int getY2() const { return _y + _h; }

protected:
	int _x;
	int _y;
	int _w;
	int _h;
};
