#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>

#include "JSON.h"

#include "Rectangle.h"
#include "Intersection.h"

#define MAX_RECTANGLES_TO_PROCESS 1000


void printUsage()
{
	std::cout << "Usage: intersector path_to_json\n";
}

void printResults(std::vector<Intersection> intersections)
{
	std::cout << "\nIntersections\n";
	if (intersections.size() > 0) {
		for (std::vector<Intersection>::iterator r = intersections.begin(); r != intersections.end(); r++) {
			std::ostringstream oss;
			std::vector<int> ids = r->getId().get();
			for (int i = 0; i < ids.size(); ) {
				oss << ids[i];

				++i;

				if (i == ids.size() - 1)
					oss << " and ";
				else if (i < ids.size() - 1)
					oss << ", ";
			}
			std::cout << "\tBetween rectangle " << oss.str() << " at (" << r->getX() << "," << r->getY() << "), w=" << r->getW() << ", h=" << r->getH() << ".\n";
		}
	}
	else
		std::cout << "\tNo intersections found\n";
	std::cout << "\n";
}

std::vector<Intersection> getIntersections(const Rectangle& rect, std::vector<Rectangle>::iterator begin, std::vector<Rectangle>::iterator end)
{
	std::vector<Intersection> intersections;

	for (std::vector<Rectangle>::iterator r = begin; r != end; r++) {
		Intersection intersection = rect.intersects(*r);

		if (intersection.isValid()) {
			intersections.push_back(intersection);
		}

		std::vector<Intersection> innerIntersections = getIntersections(intersection, r + 1, end);
		for (std::vector<Intersection>::iterator i = innerIntersections.begin(); i != innerIntersections.end(); i++) {
			intersections.push_back(*i);
		}
	}

	return intersections;
}

int main(int argc, char* argv[])
{
	if (argc != 2) {
		printUsage();
		return 1;
	}

	char * jsonPath = argv[1];

	// JSON file opening and reading
	std::ifstream file;
	file.open(jsonPath);
	if (!file.is_open()) {
		std::cout << "Error reading file\n";
		return 1;
	}

	// Get length of the file
	file.seekg(0, std::ios::end);
	int length = file.tellg();

	file.seekg(0, std::ios::beg);

	std::string buffer;
	buffer.assign((std::istreambuf_iterator<char>(file)), (std::istreambuf_iterator<char>()));

	file.close();

	JSONValue * value = JSON::Parse(buffer.c_str());

	if (value == NULL) {
		std::cout << "Error parsing JSON\n";
		return -1;
	}

	JSONObject root = value->AsObject();

	if (root.find(L"rects") == root.end()) {
		std::cout << "Error parsing JSON. 'rects' not found\n";
		return -1;
	}

	if (!root[L"rects"]->IsArray()) {
		std::cout << "Error. 'rects' is not an array\n";
		return -1;
	}

	// Parsing of each of the rectangles in the JSON (until a maximun of 1000)

	JSONArray rects = root[L"rects"]->AsArray();

	int rectangles_to_process = std::min((int) MAX_RECTANGLES_TO_PROCESS, (int) rects.size());

	std::cout << "Rectangles: \n";
	std::vector<Rectangle> rectangles;
	for (int i = 0, count = 1; i < rectangles_to_process; i++) {
		if (!rects[i]->IsObject()) continue;

		JSONObject rectangleObj = rects[i]->AsObject();

		if (rectangleObj.find(L"x") != rectangleObj.end() ||
			rectangleObj.find(L"y") != rectangleObj.end() ||
			rectangleObj.find(L"w") != rectangleObj.end() ||
			rectangleObj.find(L"h") != rectangleObj.end())
		{
			int x = rectangleObj[L"x"]->AsNumber();
			int y = rectangleObj[L"y"]->AsNumber();
			int w = rectangleObj[L"w"]->AsNumber();
			int h = rectangleObj[L"h"]->AsNumber();

			rectangles.push_back(Rectangle(count, x, y, w, h));

			std::cout << "\t" << count << ": Rectangle at (" << x << "," << y << "), w=" << w << ", h=" << h << ".\n";

			count++;
		}
	}

	std::vector<Intersection> intersections;
	for (std::vector<Rectangle>::iterator r = rectangles.begin(); r != rectangles.end() - 1; r++) {
		std::vector<Intersection> tintersections = getIntersections(*r, r+1, rectangles.end());

		for (std::vector<Intersection>::iterator iter = tintersections.begin(); iter != tintersections.end(); iter++) {
			intersections.push_back(*iter);
		}
	}

	printResults(intersections);

	return 0;
}
