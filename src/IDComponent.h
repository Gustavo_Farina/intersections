#pragma once

#include <assert.h>
#include <ostream>
#include <sstream>
#include <set>
#include <vector>

class IDComponent
{
public:
	IDComponent() {}

	IDComponent(const IDComponent& id) {
		for (std::set<int>::iterator iter = id._ids.begin(); iter != id._ids.end(); iter++) {
			_ids.insert(*iter);
		}
	}

	IDComponent(int id) {
		assert(id >= 0);
		_ids.insert(id);
	}

	void operator=(int id) {
		assert(id >= 0);
		_ids.clear();
		_ids.insert(id);
	}

	void operator=(IDComponent id) {
		_ids.clear();
		for (std::set<int>::iterator iter = id._ids.begin(); iter != id._ids.end(); iter++) {
			_ids.insert(*iter);
		}
	}

	void operator+=(int id) {
		assert(id >= 0);
		_ids.insert(id);
	}

	void operator+=(IDComponent id) {
		for (std::set<int>::iterator iter = id._ids.begin(); iter != id._ids.end(); iter++) {
			_ids.insert(*iter);
		}
	}

	std::vector<int> get() const {
		std::vector<int> ids;
		for (std::set<int>::iterator id = _ids.begin(); id != _ids.end(); id++) {
			ids.push_back(*id);
		}
		return ids;
	}

private:
	std::set<int> _ids;
};
