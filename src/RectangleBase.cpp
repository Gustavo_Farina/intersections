#include "RectangleBase.h"

#include <algorithm>
#include <assert.h>

RectangleBase::RectangleBase()
{
	_x = 0;
	_y = 0;
	_w = 0;
	_h = 0;
}

RectangleBase::RectangleBase(const RectangleBase& rect)
{
	assert(rect.getX() >= 0);
	assert(rect.getY() >= 0);
	assert(rect.getW() >= 0);
	assert(rect.getH() >= 0);

	_x = rect.getX();
	_y = rect.getY();
	_w = rect.getW();
	_h = rect.getH();
}

RectangleBase::RectangleBase(int x, int y, int w, int h)
{
	assert(x >= 0);
	assert(y >= 0);
	assert(w >= 0);
	assert(h >= 0);

	_x = x;
	_y = y;
	_w = w;
	_h = h;
}

bool RectangleBase::isValid()
{
	return !(_x == 0 && _y == 0 && _w == 0 && _h == 0);
}
