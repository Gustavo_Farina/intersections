#pragma once

#include "RectangleBase.h"

#include "IDComponent.h"

class Intersection;

class Rectangle : public RectangleBase
{
public:
	Rectangle();

	Rectangle(const Rectangle& rect);

	Rectangle(IDComponent id);

	Rectangle(int x, int y, int w, int h);

	Rectangle(IDComponent id, int x, int y, int w, int h);

	IDComponent getId() const { return _id; }

	virtual Intersection intersects(const Rectangle& rect) const;

protected:
	IDComponent _id;
};
