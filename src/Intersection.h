#pragma once

#include <set>

#include "Rectangle.h"

class Intersection : public Rectangle {
public:
	Intersection();

	Intersection(const Rectangle& rect);

	Intersection(int x, int y, int w, int h);

	Intersection(const Rectangle& rectA, const Rectangle& rectB);

	virtual Intersection intersects(const Rectangle& rect) const;

	IDComponent getId();

	void addId(IDComponent id);
};
